import sys
from PyQt5.QtWidgets import QApplication
from guiapp import GUIApp

app = QApplication(sys.argv)
pyqt = GUIApp(app)
pyqt.launch()
