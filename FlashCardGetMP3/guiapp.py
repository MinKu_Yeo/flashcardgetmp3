import platform
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QDesktopWidget
from urllib import request
from gtts import gTTS

from exceldeck import ExcelDeck
from engkor import EngKorSpider

class GUIApp(QWidget):

    def __init__(self, app):
        super().__init__()

        # Widget - w param
        self.app = app
        
        # Push Button Info
        buttonText = ['Open Deck', 'Set Download Directory', 'Get Pronounciation MP3 File' ,'Open Common Deck', 'Get Definition']
        startPosition = 320
        h1 = 20
        h2 = h1 + 30
        h3 = h2 + 30
        buttonLocation = [[startPosition, h1], [startPosition + 120, h1], [startPosition, h2], [startPosition + 180, h2], [startPosition, h3]]
        buttonConnect = [self.bConnOpenDeck, self.bConnDownDir, self.bConnDownStart, self.bConnOpenCommonDeck, self.bConnGetDefinition]
        buttonEnable = [True, True, False, False, False]
        self.buttonCondition = [False, False, False, False, False]

        checkList = [buttonLocation, buttonConnect, buttonEnable, self.buttonCondition]
        count = len(buttonText)
        for check in checkList:
            assert (count == len(check)), "Match the number of item!"

        # Init
        self.buttonList = self.createButtonList(count, buttonText, buttonLocation, buttonConnect, buttonEnable, self.buttonCondition)
        self.pushButton = self.createPushButtons(self, self.buttonList)

        self.downDir = 'temp'

        self.fileNameList = []

        self.initUI(self)

    def createButtonList(self, count, text, location, connect, enable, condition):
        retVal = []

        for k in range(count):
            temp = ButtonInfo(text[k], location[k], connect[k], enable[k], condition[k])
            retVal.append(temp)

        return retVal

    
    def createPushButtons(self, w, bList):
        retVal = []

        for b in bList:
            temp = QPushButton(w)
            temp.setText(b.text)
            temp.move(b.location[0], b.location[1])
            temp.clicked.connect(b.connect)
            temp.setEnabled(b.enable)
            retVal.append(temp)

        return retVal

    # Push Button Callback / connect functions
    def bConnOpenDeck(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filter = "Excel Files (*.xlsx)"
        defaultPath = "C:/Users/User/Dropbox/Flashcards Deluxe/English Cards"

        fileNames, _ = QFileDialog.getOpenFileNames(self, self.buttonList[0].text, defaultPath, filter, options=options)
        if fileNames:

            if len(self.fileNameList): ## List of the deck is not empty
                self.fileNameList.clear()

            for fName in fileNames:
                self.fileNameList.append(fName)

            self.bConditionUpdate(0, True)

    def bConnDownDir(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        #filter = "Excel Files (*.xlsx)"
        defaultPath = "C:/Users/User/Dropbox/Flashcards Deluxe/English Cards/1common Media"

        temp = str(QFileDialog.getExistingDirectory(self, self.buttonList[1].text, defaultPath, options=options))
        osPlatform = platform.system()
        #dir = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        if temp:
            self.downDir = self.concatDirTail(temp)
            self.bConditionUpdate(1, True)

    def bConnDownStart(self):
        assert (len(self.downDir)), "Empty download directory"
        assert (len(self.fileNameList)), "No Excel files selected"

        for fn in self.fileNameList:
            deck = ExcelDeck(fn)
            #deck.logWords()

            wordList = deck.getNoMP3WordList()
            if not wordList: # the list w is empty - there is nothing to download!
                print('Nothing to Download at file {0}'.format(fn))
                continue

            for w in wordList:
                self.downMP3andEditExcelFile(w, deck)

            deck.saveExcelFile()
            print('Saved and Finished Downloading for deck {0}'.format(fn))

    def bConnOpenCommonDeck(self):
        print('QWERTy')

    def bConnGetDefinition(self):
        for fn in self.fileNameList:
            deck = ExcelDeck(fn)
            wordList = deck.getEveryValueAtCol('A')
            row = 2
            for w in wordList:
                s = EngKorSpider(w)
                wDef = s.getDefinition()
                if not wDef: # empty string, failed to get the definition
                    wDef = 'FAILED'
                    print('Word = {0}'.format(w))
                deck.setValueAtIndex(row, 'C', wDef)
                row += 1
            deck.saveExcelFile()
        print('End of getting Def')

    def bConditionUpdate(self, instanceIndex, cond):
        self.buttonList[instanceIndex].cond = cond

        # Download MP3 condition
        if self.buttonList[0].cond == True and self.buttonList[1].cond == True:
            self.buttonList[2].enable = True
            self.pushButton[2].setEnabled(True)

        # Get Definition condtion
        if self.buttonList[0].cond == True:
            self.buttonList[4].enable = True
            self.pushButton[4].setEnabled(True)

    def downMP3andEditExcelFile(self, word, deck):
        urlBase = 'https://ssl.gstatic.com/dictionary/static/sounds/de/0/'
        fnameExtension = '.mp3'

        for attempt in range(10):
            temp = [urlBase, word, fnameExtension] # concat the URL
            url = ''.join(temp)
            ttemp = [self.downDir, word, fnameExtension]
            destName = ''.join(ttemp)
            mp3File = open(destName, 'wb') # binary write
            isDownlaoded = False

            try: # Try the Google gstatic
                response = request.urlopen(url)
                respData = response.read()
                mp3File.write(respData)
                isDownlaoded = True
                
            except request.HTTPError as e:
                #print(e.reason)
                if e.code == 404 or e.code == 400:
                    # Now, try Google TTS (Cannot find MP3 404 error / Bad Request 400)
                    try:
                        mp3File.close()
                        tts = gTTS(text=word + '!', lang='en', slow=True)
                        tts.save(destName)
                        isDownlaoded = True

                    except Exception as ee:
                        print(ee.reason)
                elif e.code == 10060: # Connection timed out, retry
                    print('Timeout error! Attempt = {0}. Retrying'.format(attempt))
                else:
                    #failedWords.append(w)
                    raise
            finally:
                mp3File.close()
                if isDownlaoded:
                    deck.setIgnoreMP3Index1(word)
                    break # end of the attempt

        # deck.saveExcelFile()
        '''if len(failedWords):
            failtxt = open(self.downDir + 'failed.txt', mode='wt', encoding='utf-8')
            failtxt.write('\n'.join(failedWords))
            print('Failed Ones:')
            print(failedWords)'''

    def concatDirTail(self, dir):
        strList = [dir, '/']
        return ''.join(strList)
    '''
    def concatDirTail(self, dir, osPlat):
        tail = self.getDirStrTail(osPlat)
        strList = [dir, tail]
        return ''.join(strList)

    def getDirStrTail(self, osPlat):
        return {
            'Windows': '\\'
            }.get(osPlat, '/') # default
    '''

    def initUI(self, w):
        title = "MP3 Pronunciation Downloader"
        width = 640
        height = 480

        w.setWindowTitle(title)
        w.resize(width, height)
        self.center(w)

    def center(self, w):
        qr = w.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        w.move(qr.topLeft())

    def launch(self):
        self.show()
        sys.exit(self.app.exec_())

class ButtonInfo():

    def __init__(self, text, location, connect, enable, cond):
        
        self.text = text
        self.location = location
        self.connect = connect
        self.enable = enable
        # self.setToolTip

        self.cond = cond

