import urllib.request
import urllib.parse
import bs4 as bs

class EngKorSpider(object):

    def __init__(self, word):
       #baseUrl = 'https://www.google.com/search?q=define+'
        self.finalUrl = ''
        self.wordLength = len(word.split())
        rootUrl = 'http://endic.naver.com/'
        firstTail = 'search.nhn?sLn=kr&searchOption=entry_idiom&query='
        # Use Word / Idion search option
        temp = self.concatString(rootUrl, firstTail)

        if self.wordLength > 1:
            word = word.replace(' ', '%20')

        firstUrl = self.concatString(temp, word)
        self.setFinalUrl(firstUrl, rootUrl)

        #if self.isDefinitionExist():
            #print('Cannot get final URL')

    def setFinalUrl(self, firstUrl, rootUrl):
        body = self.getSoupBody(firstUrl)
        q0 = self.getQueryType(self.wordLength)
        q1 = '&query='

        for link in body.find_all('a'):
            target = link.get('href')
            if self.isThisSubstringExist(target, q0) and self.isThisSubstringExist(target, q1):
                self.finalUrl = self.concatString(rootUrl, target[1:])
                #print(self.finalUrl)
                break


    def getQueryType(self, wordLength):
        qLen1 = 'enkrEntry.nhn?entryId='
        qIdiom = 'enkrIdiom.nhn?'
        return {
            1: qLen1,
            }.get(wordLength, qIdiom)

    def isDefinitionExist(self):
        return len(self.finalUrl) <= 1

    def getSoupBody(self, url):
        try:
            sauce = urllib.request.urlopen(url).read()
            soup = bs.BeautifulSoup(sauce, 'lxml')
            return soup.body
        except Exception as e:
            print(str(e))

    def concatString(self, base, word):
        strList = [base, word]
        return ''.join(strList)

    def getDefofSingleWord(self, body):
        retVal = ''
        dtList = body.find_all('dt')
        index = 0
        indexLast = 0
        length = len(dtList)

        for pOs in body.find_all('h3'): # 8 part of speech; verb / noun / ...
            if str(pOs).find('fnt_syn') >= 0:
                retVal = self.concatString(retVal, '<')
                retVal = self.concatString(retVal, pOs.text)
                retVal = self.concatString(retVal, '>')
                parentAttr = pOs.parent.attrs

                while index < length: # meaning
                    m = dtList[index]
                    if self.isSinglewordConditionTrue(m, parentAttr):
                        trimmed = self.getTrimmedResult(m.text)
                        retVal = self.concatString(retVal, trimmed)
                        indexLast = index
                        #print(trimmed)
                    index = index + 1

                index = indexLast + 1
        
        retVal = retVal[:-1] # remove the last \n
        return retVal

    def getDefofIdiom(self, body):
        retVal = ''

        # idioms like embark on have part of speech - verb
        for m in body.find_all('dt'): # <dt class
            if self.isIdiomConditionTrue(m):
                trimmed = self.getTrimmedResult(m.text)
                retVal = self.concatString(retVal, trimmed)

        return retVal

    def getDefinition(self):
        body = self.getSoupBody(self.finalUrl)
        
        if self.isDefinitionExist():
            print('Unable to get definition')
            return ''

        if self.wordLength == 1:
            retVal = self.getDefofSingleWord(body)

        else:
            retVal = self.getDefofIdiom(body)

        return retVal

    def isThisSubstringExist(self, target, sub):
        return target.find(sub) >- 0

    def isSatisfyConditions(self, caseList):
        retVal = caseList[0]
        for c in caseList:
            retVal &= c
        return retVal

    def isSinglewordConditionTrue(self, m, parentAttr):
        string = str(m)

        # Case that has example sentence
        #case0 = [self.isThisSubstringExist(string, 'arrow_mean arrow_mean_off')]
        #retVal = self.isSatisfyConditions(case0)

        # Or check case that has no example sentence
        #case1 = [self.isThisSubstringExist(string, 'fnt_k06'), self.isThisSubstringExist(string, 'itt_button')]
        #retVal |= self.isSatisfyConditions(case1)

        # And check parent attributes
        caseP = [m.parent.parent.attrs == parentAttr]
        #retVal &= self.isSatisfyConditions(caseP)
        retVal = self.isSatisfyConditions(caseP)
        return retVal

    def getTrimmedResult(self, text):
        noNextLine = text.replace('\n', ' ')
        trimmed = noNextLine.replace(' 예문닫기', '')
        retVal = self.concatString(trimmed, '\n')
        return retVal

    def isIdiomConditionTrue(self, m):
        retVal = False

        # first meaning
        retVal = self.isThisSubstringExist(str(m), 'mean_on')

        # other meanings, but no Eng-Eng dictionary
        c1 = [self.isThisSubstringExist(str(m), 'em class'), not self.isThisSubstringExist(str(m), 'first\"')]
        retVal |= self.isSatisfyConditions(c1)
        return retVal

if __name__ == "__main__":

    list = [' be subjected to', 'account for', 'inherent in', 'cope with', 'embark on', 'accord', 'abundance', 'potentially', 'attributable to']

    for w in list:
        print('\nWord = {0}'.format(w))
        temp = EngKorSpider(w)
        print(temp.getDefinition())

