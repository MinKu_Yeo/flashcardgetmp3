import re
from xlutils.copy import copy
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

class ExcelDeck(object):
    
    def __init__(self, fileName):
        # do not get formula value from the Excel - use data_only
        # but this will lose the formulae
        self.workbook = load_workbook(fileName)
        self.fileName = fileName
        self.sheetName = 'Sheet1'
        sheets = self.workbook.get_sheet_names()
        self.worksheet = self.getWorksheet(sheets)
        self.total = self.getTotalWordCount()

        self.colText1 = self.getIndexOfColumn('Text 1')
        self.colText2 = self.getIndexOfColumn('Text 2')
        self.colSound1 = self.getIndexOfColumn('Sound 1')
        self.colSound2 = self.getIndexOfColumn('Sound 2')

        self.indexIgnoreSound1 = 'F' # use combox box or something in the future!

    def saveExcelFile(self):
        self.workbook.save(self.fileName)

    def getWorksheet(self, sheets):
        sname = ''
        for s in sheets:
            if s == self.sheetName:
                sname = s
                break
        return self.workbook.get_sheet_by_name(sname)

    def getIndexOfValue(self, value, rowLimit=None):
        # for example, this returns A1, C3, ...
        retVal = ''
        if rowLimit is None:
            rowLimit = self.worksheet.max_row

        for col in range(1, self.worksheet.max_column + 1):
            colLetter = get_column_letter(col)

            for row in range(1, rowLimit + 1):
                index = self.getIndex(row, colLetter)
                compare = self.getValueAtIndex(row, colLetter)
                if compare == value:
                    retVal = index
                    break

        assert (len(retVal)), "Unable to find the value %r" % value
        return retVal

    def getIndexOfColumn(self, value):
        # return only the Alphabet of the column
        return self.getIndexOfValue(value=value, rowLimit=1)[0]

    def getIndex(self, row, colLetter):
        return colLetter + str(row)

    def getTotalWordCount(self):
        # or use self.worksheet.max_row
        # Since Excel's index starts at 1, do not subtract it
        return len(self.worksheet['A'])

    def logWords(self):
        mp3List = self.getEveryValueAtCol(self.colSound1)
        wordList = self.getEveryValueAtCol(self.colText1)
        length = len(wordList)

        for k in range(length):
            print('Row{0}.\tword: {1}\t mp3: {2}'.format(k+2, wordList[k], mp3List[k]))

    def getEveryValueAtCol(self, colAlphabet):
        retVal = []
        k = 2
        
        while k <= self.total:
            value = self.getValueAtIndex(k, colAlphabet)
            retVal.append(value)
            k += 1
        return retVal

    def getValueAtIndex(self, row, col):
        index = self.getIndex(row, col)
        return self.worksheet[index].value

    def setValueAtIndex(self, row, col, val):
        index = self.getIndex(row, col)
        self.worksheet[index].value = val

    def getNoMP3WordList(self):
        retVal = []
        k = 2
        haveChanged = False
        
        while k <= self.total:
            
            mp3IgnoreValue = self.getValueAtIndex(k, self.indexIgnoreSound1)

            if not mp3IgnoreValue: # there is no integer value in the ignore cell
                
                word = self.getValueAtIndex(k, self.colText1)
                #wordCount = len(word.split())
                lastChar = word[-1:]

                if ' ' == lastChar or '\n' == lastChar:
                    # there is a space at the end
                    # word = word.replace(' ', '')
                    word = word[:-1]
                    self.setValueAtIndex(k, self.colText1, word)
                    haveChanged = True
                        
                retVal.append(word)
            k += 1

        if haveChanged:
            # length 1 words that had space do not have space anymore, save the file 
            self.saveExcelFile()
        
        return retVal

    def setIgnoreMP3Index1(self, word):
        fullIndex = self.getIndexOfValue(word)
        # Extract positive integers using re(Regular Expression)
        temp = re.findall('\d+', fullIndex)
        row = int(temp[0])
        # fill the ignore value to one
        self.setValueAtIndex(row, self.indexIgnoreSound1, 1)
        #self.saveExcelFile()




