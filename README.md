# README #

Using Visual Studio Community 2017 and Python 3.6 to download MP3 files which will be filled for the Flashcards Deluxe decks

### WARNING ###
* The author is using Python for the first time !!!!!!!
* So please, comment and criticize my dirty codes !!!!!

### What is this repository for? ###

* This repository is useful for [Flashcards Deluxe app](http://orangeorapple.com/Flashcards/) users.
* The author loves the pronunciation produced by Google and Oddcast.
* So the author is creating a way to download bunch of MP3 files, which is the pronunciation of a certain word
* This program will open an Excel file, which corresponds to a card deck of the Flashcards Deluxe app, and download the pronunciation of the words on the column.

### LICENSE ###
* TBD

### The author used ###
* [Visual Studio 2017 Community](https://www.visualstudio.com/vs/python/); Because I'm a noob in Python and I needed a decent debugger even though this is slow!
* [Git LFS](https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html) for versioning Excel files
* urllib
* beautifulsoup 4.6.0
* openpyxl 2.4.8
* [gTTS 1.2.0](https://pypi.python.org/pypi/gTTS)
* PyQt5 5.8

### Ref. urllib ###
* [Python 3 Programming Tutorial - urllib module by sentdex (YouTube)](https://www.youtube.com/watch?v=5GzVNi0oTxQ&index=37&list=PLQVvvaa0QuDe8XSftW-RAxdo6OmaeL85M)

### Ref. Beautiful Soup4 ###
* [Web scraping and parsing with Beautiful Soup by sentdex (YouTube)](https://www.youtube.com/watch?v=aIPqt-OdmS0&list=PLQVvvaa0QuDfV1MIRBOcqClP6VZXsvyZS)

### Ref. PyQt5 ###
* [PyQt5 Lesson by Mark Winfield (YoutTube)](https://www.youtube.com/watch?v=zebyBJVZ2zU&list=PLZocUikpczs-Yud2lyFpSNQOvxuPUVBDp&index=11)
* [PyQt5 File Dialog by Frank A. (YoutTube)](https://www.youtube.com/watch?v=vOFPXVYsMys&index=6&list=PLlgoYPTU6ljCQV39sX-FBizFo2bcjwtB5)
* [pyqt5 center window by pythonprogramminglanguage.com](http://pythonprogramminglanguage.com/pyqt5-center-window/)
* [PyQt5 file dialog by pythonspot.com](https://pythonspot.com/en/pyqt5-file-dialog/)

### Ref. Excel & Python ###
* [Advanced Python 8: openpyxl (YouTube)](https://www.youtube.com/watch?v=A3VRd22fHhA)
* [Reading Excel sheets using xlrd by Safaa Al-Hayali (YouTube)](https://www.youtube.com/watch?v=lBDWqu4Vez4)
* [Writing Excel sheets using xlrd by Safaa Al-Hayali (YouTube)](https://www.youtube.com/watch?v=FbWnuO7GGBQ)
* [Update existing excel file using xlrd-xlwt-xlutils by Safaa Al-Hayali (YouTube)](https://www.youtube.com/watch?v=38XhSpmuXQw)
